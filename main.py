#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write(self.portfolio())
    def portfolio(self):
        st="";
        st+=self.get_header();
        st+=self.get_body();
        return st;
    def get_header(self):
        st="<!DOCTYPE html>";
        st+="""<html lang="en">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Portfolio for Momen Bhuiyan">
	<meta name="keywords" content="Momen, Bhuiyan, Portfolio">
	<meta name="author" content="Momen Bhuiyan">
	<title>About Me | Momen Bhuiyan</title>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400" rel="stylesheet">
    <style type="text/css">p{line-height: 130%;font-family:'Roboto',sans-sherif;}a:hover{background:lightyellow;}</style>
</head>
""";
        return st;
    def get_body(self):
        st=""
        st+="<body><h3>About Me</h3>";
        st+="""<p>My name is <span style="font-weight:bold;">Md. Momen Bhuiyan</span>. I am a Jr. Software Engineer at <a href="revesoft.com" target="_blank">REVE Systems Ltd.</a>. I have finished my undergraduate study at Bangladesh University of Engineering and Technology(<a href="http://www.buet.ac.bd/" target="_blank">BUET</a>). I am a CS major. My research interests are machine learning especially in deep learning and reinforcement learning.""";
        st+="""I have completed my undergraduate thesis under the supervision of <a href="http://cse.buet.ac.bd/faculty/facdetail.php?id=mdmonirulislam" target="_blank">Prof. Md. Monirul Islam</a>. My thesis topic was modifying a <a href="http://en.wikipedia.org/wiki/Cluster_analysis#Density-based_clustering" target="_blank">density-based</a> <a href="http://en.wikipedia.org/wiki/Clustering_high-dimensional_data#Subspace_clustering" target="_blank">Subspace Clustering</a> algorithm. We have worked on improving <a href="http://www.dbs.ifi.lmu.de/Publikationen/Papers/boehmc_subspace.pdf" target="_blank">PreDeCon</a>. For the detailed thesis see <a href="/ugradthesis">this</a>. Other than that I have also worked on a research project on Distributed Traffic Simulator for heterogeneous network based on Dhaka city traffic with <a href="http://cse.buet.ac.bd/faculty/facdetail.php?id=himeldev" target="_blank">Himel Dev</a> under <a href="http://cse.buet.ac.bd/faculty/facdetail.php?id=razi" target="_blank">Dr. A. B. M. Alim Al Islam</a>. Detail for this project can be found <a href="https://sites.google.com/site/dhakasim/home" target="_blank">here</a>.</p>""";
        st+="""<p>I like reading, especially thriller and science fiction. I have participated in several contest during my undergraduate. My team became 3rd in <a href="http://dnet.org.bd/page/Generic/0/61/145/54/39" target="_blank">CFICC</a>. We got best demo award <a href="https://sites.google.com/site/abmalimalislam/achievements" target="_blank">MoHCI</a>. I have also helped build these sites(<a href="http://buetech.com/" target="_blank">BUETECH</a>, <a href="http://mojadar.com/" target="_blank">Mojadar</a>).""";
        st+="""I am not very active in online sites. Still here are some links <a href="https://github.com/lnman/" target="_blank">@github</a>, <a href="http://stackoverflow.com/users/5421595/nomem" target="_blank">@stackoverflow</a>. My programming competition handles are : <a href="http://codeforces.com/profile/lnman" target="_blank">@codeforces</a>, <a href="https://www.topcoder.com/members/nomem/" target="_blank">@topcoder</a> .My social network profiles are: <a href="https://www.facebook.com/mimonnn" target="_blank">@facebook</a>, <a href="https://www.linkedin.com/pub/momen-bhuiyan" target="_blank">@linkedin</a>.</p>""";
        st+="""<p>My resume for academia is <a href="https://drive.google.com/open?id=0BykCIVowVfASOVM2OGJkWXFDMDg">here</a> and for job is <a href="https://drive.google.com/open?id=1RtdXis4GknIuC3Ltv3dYUUjnWyNar7RnZkQrXSDSlRY">here</a>.If any link is broken contact me at <<a href="mailto:momen_bhuiyan@yahoo.com" target="_blank" style="text-decoration: none;">momen_bhuiyan@yahoo.com</a>> or <<a href="mailto:lnman000666@gmail.com" target="_blank" style="text-decoration: none;">lnman000666@gmail.com</a>> .</p>"""
        st+="""<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63913865-1', 'auto');
  ga('send', 'pageview');

</script>
        """;
        st+="""</body></html>""";
        return st;



class ThesisHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write(self.portfolio())
    def portfolio(self):
        st="";
        st+=self.get_header();
        st+=self.get_body();
        return st;
    def get_header(self):
        st="<!DOCTYPE html>";
        st+="""<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Portfolio for Momen Bhuiyan">
    <meta name="keywords" content="Momen, Bhuiyan, Portfolio">
    <meta name="author" content="Momen Bhuiyan">
    <title>About Me | Momen Bhuiyan</title>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico">
    <style type="text/css">p{line-height: 130%;}a:hover{background:lightyellow;}</style>
</head>
""";
        return st;
    def get_body(self):
        st=""
        st+="<body><h1>Undergraduate Thesis</h1>";
        st+="""<p>My name is <span style="font-weight:bold;">Momen Bhuiyan</span>. I am a 4th year undergraduate student at Bangladesh University of Engineering and Technology(<a href="http://www.buet.ac.bd/" target="_blank">BUET</a>). I am a CS major. This is an attempt to an about me.</p>""";
        st+="""<p>I am doing my thesis under the supervision of <a href="http://cse.buet.ac.bd/faculty/facdetail.php?id=mdmonirulislam" target="_blank">Prof. Md. Monirul Islam</a>. My thesis topic is <a href="http://en.wikipedia.org/wiki/Cluster_analysis#Density-based_clustering" target="_blank">Density-based</a> <a href="http://en.wikipedia.org/wiki/Clustering_high-dimensional_data#Subspace_clustering" target="_blank">Subspace Clustering</a>. We are working on improving <a href="http://www.dbs.ifi.lmu.de/Publikationen/Papers/boehmc_subspace.pdf" target="_blank">PreDeCon</a>. Other than that I am also working on a research project on Distributed Traffic Simulator for heterogeneous network based on Dhaka city traffic with <a href="http://cse.buet.ac.bd/faculty/facdetail.php?id=himeldev" target="_blank">Himel Dev</a> under <a href="http://cse.buet.ac.bd/faculty/facdetail.php?id=razi" target="_blank">Dr. A. B. M. Alim Al Islam</a>.</p>""";
        st+="""<p>I like reading, especially thriller and science fiction. I am kind of a computer nerd. I like doing stuff that are meant for nothing. My team became 3rd in <a href="http://dnet.org.bd/page/Generic/0/61/145/54/39" target="_blank">CFICC</a>. I also participated in some other competition. I got best demo award <a href="https://sites.google.com/site/abmalimalislam/achievements" target="_blank">MoHCI</a>. I also helped build these sites(<a href="http://buetech.com/" target="_blank">BUETECH</a>, <a href="http://mojadar.com/" target="_blank">Mojadar</a>).</p>""";
        st+="""<p>Somethings that I started but never finished can be  found <a href="https://github.com/lnman/" target="_blank">@github</a>. My programming competition handles are : <a href="http://codeforces.com/profile/lnman" target="_blank">@codeforces</a>, <a href="http://www.topcoder.com/tc?module=MemberProfile&cr=23113767" target="_blank">@topcoder</a> .My social network profiles are: <a href="https://www.facebook.com/mimonnn" target="_blank">@facebook</a>, <a href="https://plus.google.com/u/0/108682863638118081611" target="_blank">@google-plus</a>, <a href="https://www.linkedin.com/pub/momen-bhuiyan" target="_blank">@linkedin</a>.</p>""";
        st+="""<p>And lastly contact me at <<a href="mailto:momen_bhuiyan@yahoo.com" target="_blank" style="text-decoration: none;">momen_bhuiyan@yahoo.com</a>> or <<a href="mailto:lnman000666@gmail.com" target="_blank" style="text-decoration: none;">lnman000666@gmail.com</a>> .</p>\n"""
        st+="""<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63913865-1', 'auto');
  ga('send', 'pageview');

</script>
        """;
        st+="""</body></html>""";
        return st;

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/ugradthesis', ThesisHandler)
], debug=True)
